# Javascript Ninja #

## Задание 3 ##

Самостоятельное тестирование функций createSmartObject и defineComputedField

## Что следать? ##

* Скопировать себе репозиторий.
* В консоли выполнить команду ```npm install``` для устаноки необходимых пакетов
* В файл `./task_1/computed.js` вставить своё решение

## Как тестировать? ##

Запустить `index.html`. Перейти по ссылке `Задание 3.1` или `Задание 3.2`. В правом верхнем углу страницы будет виден краткий результат тестирования. В самом низу страницы будет подробный результат тестирования.

## Пример успешного тестирования ##
![Test|Successful](./img/test-successful.jpg)

## Пример неуспешного тестирования ##
![Test|Failed](./img/test-fail.jpg)